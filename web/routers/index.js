const route = require('koa-router')
const router = new route()
const userinfo = require('./userinfo.js')
const logout = require('./logout.js')


router.post('/userinfo', userinfo)
router.get('/userinfo', userinfo)
router.post('/logout', logout)

module.exports = router
