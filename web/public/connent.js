var mysql  = require('mysql');  
 
var connection = mysql.createConnection({     
  host     : 'localhost',       
  user     : 'root',              
  password : '',       
  port: '3306',                   
  database: 'app', 
}); 
 
connection.connect()


module.exports = (sql) => {
  console.log(sql)
  return new Promise((resolve, reject) => {
    // 执行sql
    connection.query(sql, (err, result) => {
      // 关闭连接
      if (err)  reject(err) 
      resolve(JSON.parse(JSON.stringify(result)))
    })
  })
 
  // 关闭连接 
  // connection.end()
}