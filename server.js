const Koa = require('koa')
const static = require('koa-static')
const path = require('path')
const router = require('./web/routers/index.js')
const cors = require('koa-cors')
const koaBody = require('koa-body')

console.log(router)
const app = new Koa()

// 允许跨域
app.use(cors())
// post参数获取
app.use(koaBody())

// 路由
app.use(router.routes())

// 静态资源
app.use(static(path.join(__dirname)));


app.listen(8001)